## About

This app is a test task. It is a currency exchange app based on MVVM architecture with usage of Hilt
DI, Room DB and Retrofit2.

## Stack

This app was made using:

- Kotlin
- Hilt DI
- Coroutines Flow
- Room DB
- Retrofit2
- Jetpack Compose

## Features

The android app lets you:

- Exchange currency locally with DB implementation
- Calculate currency before exchange