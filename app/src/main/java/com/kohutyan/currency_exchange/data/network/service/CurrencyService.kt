package com.kohutyan.currency_exchange.data.network.service

import com.kohutyan.currency_exchange.data.network.model.responses.CurrencyRateResponse
import retrofit2.http.GET

interface CurrencyService {
    @GET(CURRENCY_EXCHANGE_RATES)
    suspend fun getExchangeRates(): CurrencyRateResponse

    companion object {
        const val CURRENCY_EXCHANGE_RATES = "currency-exchange-rates"
    }
}