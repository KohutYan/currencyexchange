package com.kohutyan.currency_exchange.data.db.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.kohutyan.currency_exchange.data.db.entities.BALANCE_TABLE
import com.kohutyan.currency_exchange.data.db.entities.BalanceEntity
import kotlinx.coroutines.flow.Flow

@Dao
interface UserBalanceDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(logItem: BalanceEntity)

    @Query("SELECT * FROM $BALANCE_TABLE")
    fun getAll(): Flow<List<BalanceEntity>>

    @Query("SELECT * FROM $BALANCE_TABLE WHERE currency=:id")
    suspend fun getBalanceById(id: String): BalanceEntity?
}