package com.kohutyan.currency_exchange.data.features.currency

import com.kohutyan.currency_exchange.data.network.model.responses.CurrencyRateResponse
import com.kohutyan.currency_exchange.data.network.service.CurrencyService
import com.kohutyan.currency_exchange.domain.features.currency.ICurrencyRepository
import javax.inject.Inject

class CurrencyRepository @Inject constructor(
    private val service: CurrencyService
) : ICurrencyRepository {
    override suspend fun getCurrencyRate(): CurrencyRateResponse {
        return service.getExchangeRates()
    }
}