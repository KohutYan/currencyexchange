package com.kohutyan.currency_exchange.data.network.model.responses

import java.util.Date

data class CurrencyRateResponse(
    val base: String,
    val date: Date,
    val rates: Map<String, Double>
)