package com.kohutyan.currency_exchange.data.db

import androidx.room.Database
import androidx.room.RoomDatabase
import com.kohutyan.currency_exchange.data.db.dao.UserBalanceDao
import com.kohutyan.currency_exchange.data.db.entities.BalanceEntity

@Database(
    version = 1, entities = [
        BalanceEntity::class
    ]
)
abstract class AppDatabase : RoomDatabase() {
    abstract fun userBalanceDao(): UserBalanceDao

    companion object {
        const val DB_NAME = "CE_DB"
    }
}