package com.kohutyan.currency_exchange.data.db.entities

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.kohutyan.currency_exchange.domain.models.Balance

@Entity(tableName = BALANCE_TABLE)
data class BalanceEntity(
    @PrimaryKey
    val currency: String,
    val value: Double
)

fun BalanceEntity.toBalance() = Balance(this.currency, this.value)

const val BALANCE_TABLE = "BALANCE"