package com.kohutyan.currency_exchange.data.features.balance

import com.kohutyan.currency_exchange.data.db.dao.UserBalanceDao
import com.kohutyan.currency_exchange.data.db.entities.BalanceEntity
import com.kohutyan.currency_exchange.domain.exceptions.MissingBalanceException
import com.kohutyan.currency_exchange.domain.features.balance.IBalanceRepository
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

class BalanceRepository @Inject constructor(
    private val balanceDao: UserBalanceDao
) : IBalanceRepository {
    override suspend fun getBalancesFromDB(): Flow<List<BalanceEntity>> {
        return balanceDao.getAll()
    }

    override suspend fun getBalanceFromDB(currencyName: String): BalanceEntity {
        return balanceDao.getBalanceById(currencyName)
            ?: throw MissingBalanceException("No balance of currency $currencyName found")
    }

    override suspend fun addBalanceToDB(balance: BalanceEntity) {
        return balanceDao.insert(balance)
    }

}