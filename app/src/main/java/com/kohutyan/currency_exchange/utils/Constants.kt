package com.kohutyan.currency_exchange.utils

const val DEFAULT_CURRENCY = "EUR"
const val DEFAULT_BALANCE = 10000.0

const val DATE_FORMAT = "yyyy-MM-dd"
const val EMPTY_STRING = ""

const val TWO_AFTER_COMMA = "%.2f"

const val DEFAULT_FEE = 0.07
