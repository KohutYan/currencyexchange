package com.kohutyan.currency_exchange.utils.extensions

import kotlin.math.roundToInt
import kotlin.math.pow

fun Double.roundDecimalPart(length: Int): Double {
    assert(length >= 0)

    val multiplier = 10.0.pow(length)

    return (this * multiplier).roundToInt() / multiplier
}