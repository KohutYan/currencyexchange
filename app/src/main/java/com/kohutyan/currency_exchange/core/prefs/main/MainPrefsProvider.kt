package com.kohutyan.currency_exchange.core.prefs.main

import android.content.Context
import com.google.gson.Gson
import com.kohutyan.currency_exchange.core.platform.providers.BasePrefsProvider
import javax.inject.Inject

class MainPrefsProvider @Inject constructor(
    context: Context,
    gson: Gson,
) : BasePrefsProvider(context, gson), IMainPrefs {

    override fun isFirstLaunch(): Boolean {
        val value = getPreference(IS_FIRST_LAUNCH, true)

        if (value) {
            setPreference(IS_FIRST_LAUNCH, false)
        }

        return value
    }

    companion object {
        const val IS_FIRST_LAUNCH = "IS_FIRST_LAUNCH"
    }
}