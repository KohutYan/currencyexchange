package com.kohutyan.currency_exchange.core.prefs.fee

import android.content.Context
import com.google.gson.Gson
import com.kohutyan.currency_exchange.core.platform.providers.BasePrefsProvider
import javax.inject.Inject

class FeePrefsProvider @Inject constructor(
    context: Context,
    gson: Gson,
) : BasePrefsProvider(context, gson), IFeePrefs {

    override fun hasFee(): Boolean {
        val prev = getPreference(TRANSACTIONS_COUNT_PREFS, 0)
        return prev > NO_FEE_TRANSACTIONS
    }

    override fun addTransaction() {
        val prev = getPreference(TRANSACTIONS_COUNT_PREFS, 0)
        setPreference(TRANSACTIONS_COUNT_PREFS, prev + 1)
    }

    companion object {
        const val TRANSACTIONS_COUNT_PREFS = "TRANSACTIONS_COUNT"
        const val NO_FEE_TRANSACTIONS = 5
    }
}