package com.kohutyan.currency_exchange.core.prefs.main

interface IMainPrefs {
    fun isFirstLaunch(): Boolean
}