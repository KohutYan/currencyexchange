package com.kohutyan.currency_exchange.core.di.modules

import com.kohutyan.currency_exchange.data.db.dao.UserBalanceDao
import com.kohutyan.currency_exchange.data.features.balance.BalanceRepository
import com.kohutyan.currency_exchange.data.features.currency.CurrencyRepository
import com.kohutyan.currency_exchange.data.network.service.CurrencyService
import com.kohutyan.currency_exchange.domain.features.balance.IBalanceRepository
import com.kohutyan.currency_exchange.domain.features.currency.ICurrencyRepository
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import retrofit2.Retrofit
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
class RepositoryModule {
    @Provides
    @Singleton
    fun provideCurrencyRepo(
        retrofit: Retrofit
    ): ICurrencyRepository =
        CurrencyRepository(retrofit.create(CurrencyService::class.java))

    @Provides
    @Singleton
    fun provideBalanceRepo(
        balanceDao: UserBalanceDao
    ): IBalanceRepository =
        BalanceRepository(balanceDao)
}