package com.kohutyan.currency_exchange.core.prefs.fee

interface IFeePrefs {
    fun hasFee(): Boolean
    fun addTransaction()
}