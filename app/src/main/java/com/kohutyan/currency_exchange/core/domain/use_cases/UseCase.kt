package com.kohutyan.currency_exchange.core.domain.use_cases

import com.kohutyan.currency_exchange.core.domain.use_cases.UseCase.None

abstract class UseCase<out Type, in Params> where Type : Any? {

    abstract fun run(params: Params): Type

    class None
}

fun none() = None()