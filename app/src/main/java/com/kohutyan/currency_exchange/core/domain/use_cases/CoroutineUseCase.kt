package com.kohutyan.currency_exchange.core.domain.use_cases

abstract class CoroutineUseCase<out Type, in Params> where Type : Any? {

    abstract suspend fun run(params: Params): Type
}
