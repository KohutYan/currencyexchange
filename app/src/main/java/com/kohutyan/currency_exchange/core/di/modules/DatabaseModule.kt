package com.kohutyan.currency_exchange.core.di.modules

import android.content.Context
import androidx.room.Room
import com.kohutyan.currency_exchange.data.db.AppDatabase
import com.kohutyan.currency_exchange.data.db.dao.UserBalanceDao
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
class DatabaseModule {
    @Provides
    @Singleton
    fun provideDatabase(
        @ApplicationContext context: Context
    ): AppDatabase = Room.databaseBuilder(
        context,
        AppDatabase::class.java,
        AppDatabase.DB_NAME
    ).build()

    @Provides
    @Singleton
    fun provideBalanceDao(
        database: AppDatabase
    ): UserBalanceDao = database.userBalanceDao()

}