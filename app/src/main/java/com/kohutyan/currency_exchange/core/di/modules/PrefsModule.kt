package com.kohutyan.currency_exchange.core.di.modules

import android.content.Context
import com.google.gson.Gson
import com.kohutyan.currency_exchange.core.prefs.fee.*
import com.kohutyan.currency_exchange.core.prefs.main.IMainPrefs
import com.kohutyan.currency_exchange.core.prefs.main.MainPrefsProvider
import dagger.*
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent

@Module
@InstallIn(SingletonComponent::class)
class PrefsModule {
    @Provides
    fun provideMainPrefs(@ApplicationContext context: Context, gson: Gson): IMainPrefs =
        MainPrefsProvider(context, gson)

    @Provides
    fun provideFeePrefs(@ApplicationContext context: Context, gson: Gson): IFeePrefs =
        FeePrefsProvider(context, gson)

}