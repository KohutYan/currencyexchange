package com.kohutyan.currency_exchange.domain.features.balance

import com.kohutyan.currency_exchange.data.db.entities.BalanceEntity
import kotlinx.coroutines.flow.Flow

interface IBalanceRepository {
    suspend fun getBalancesFromDB(): Flow<List<BalanceEntity>>
    suspend fun getBalanceFromDB(currencyName: String): BalanceEntity
    suspend fun addBalanceToDB(balance: BalanceEntity)
}