package com.kohutyan.currency_exchange.domain.models

data class Currency(val name: String, val value: Double)