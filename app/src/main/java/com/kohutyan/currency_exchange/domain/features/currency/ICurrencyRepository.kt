package com.kohutyan.currency_exchange.domain.features.currency

import com.kohutyan.currency_exchange.data.network.model.responses.CurrencyRateResponse

interface ICurrencyRepository {

    suspend fun getCurrencyRate(): CurrencyRateResponse
}