package com.kohutyan.currency_exchange.domain.features.balance.use_cases

import com.kohutyan.currency_exchange.core.domain.use_cases.UseCase
import com.kohutyan.currency_exchange.domain.models.Currency
import javax.inject.Inject

class CountReceiveBySell @Inject constructor() : UseCase<Double, ReceiveBySellParams>() {
    override fun run(params: ReceiveBySellParams): Double {
        return (params.sell / params.sellCurrency.value) * params.receiveCurrency.value
    }
}

data class ReceiveBySellParams(
    val sell: Double,
    val sellCurrency: Currency,
    val receiveCurrency: Currency,
)
