package com.kohutyan.currency_exchange.domain.features.balance.use_cases

import com.kohutyan.currency_exchange.core.domain.use_cases.CoroutineUseCase
import com.kohutyan.currency_exchange.core.prefs.fee.IFeePrefs
import com.kohutyan.currency_exchange.domain.exceptions.MissingBalanceException
import com.kohutyan.currency_exchange.domain.exceptions.NotEnoughBalanceException
import com.kohutyan.currency_exchange.domain.models.Balance
import com.kohutyan.currency_exchange.domain.models.Currency
import com.kohutyan.currency_exchange.utils.DEFAULT_FEE
import com.kohutyan.currency_exchange.utils.extensions.roundDecimalPart
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import javax.inject.Inject

class ExchangeCurrency @Inject constructor(
    private val feePrefs: IFeePrefs,
    private val countReceiveBySell: CountReceiveBySell,
    private val getBalanceFromDb: GetBalanceFromDb,
    private val addBalanceToDb: AddBalanceToDb,
) : CoroutineUseCase<Flow<Result>, Params>() {
    override suspend fun run(params: Params): Flow<Result> = flow {
        val hasFee = feePrefs.hasFee()

        val feeMultiplier = if (hasFee) DEFAULT_FEE else 0.0

        val receive = countReceiveBySell.run(
            ReceiveBySellParams(
                params.sell, params.sellCurrency, params.receiveCurrency
            )
        ).roundDecimalPart(2)

        val sellBalance = getBalanceFromDb.run(params.sellCurrency.name)

        val receiveBalance = try {
            getBalanceFromDb.run(params.receiveCurrency.name).value
        } catch (e: Exception) {
            if (e is MissingBalanceException) {
                0.0
            } else {
                throw e
            }
        }

        val fee = (params.sell * feeMultiplier)

        val sellMinus = params.sell + fee

        if (sellBalance.value < sellMinus) throw NotEnoughBalanceException("NOT ENOUGH CASH!")

        addBalanceToDb.run(
            Balance(
                params.sellCurrency.name, (sellBalance.value - sellMinus.roundDecimalPart(2))
            )
        )

        addBalanceToDb.run(
            Balance(
                params.receiveCurrency.name, receiveBalance + receive
            )
        )

        feePrefs.addTransaction()

        emit(
            Result(
                params.sell.roundDecimalPart(2),
                receive,
                fee.roundDecimalPart(2)
            )
        )
    }
}

data class Params(
    val sell: Double,
    val sellCurrency: Currency,
    val receiveCurrency: Currency,
)

data class Result(
    val sell: Double,
    val receive: Double,
    val fee: Double,
)

/*
* check if it has fee
* if it has fee - calculate fee
*
* then remove from one balance (with or without fee)
* add to other balance
* return result with sell, receive and fee
*/