package com.kohutyan.currency_exchange.domain.models

import com.kohutyan.currency_exchange.data.db.entities.BalanceEntity

data class Balance(val name: String, val value: Double)

fun Balance.toEntity() = BalanceEntity(this.name, this.value)