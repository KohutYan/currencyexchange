package com.kohutyan.currency_exchange.domain.features.balance.use_cases

import com.kohutyan.currency_exchange.core.domain.use_cases.CoroutineUseCase
import com.kohutyan.currency_exchange.domain.features.balance.IBalanceRepository
import com.kohutyan.currency_exchange.domain.models.Balance
import com.kohutyan.currency_exchange.domain.models.toEntity
import javax.inject.Inject

class AddBalanceToDb @Inject constructor(
    private val balanceRepository: IBalanceRepository
) : CoroutineUseCase<Unit, Balance>() {
    override suspend fun run(params: Balance) {
        return balanceRepository.addBalanceToDB(params.toEntity())
    }
}