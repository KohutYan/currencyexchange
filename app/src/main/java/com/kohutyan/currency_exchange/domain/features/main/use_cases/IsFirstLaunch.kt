package com.kohutyan.currency_exchange.domain.features.main.use_cases

import com.kohutyan.currency_exchange.core.domain.use_cases.UseCase
import com.kohutyan.currency_exchange.core.prefs.main.IMainPrefs
import javax.inject.Inject

class IsFirstLaunch @Inject constructor(
    private val mainPrefsProvider: IMainPrefs
) : UseCase<Boolean, Unit>() {
    override fun run(params: Unit): Boolean {
        return mainPrefsProvider.isFirstLaunch()
    }
}