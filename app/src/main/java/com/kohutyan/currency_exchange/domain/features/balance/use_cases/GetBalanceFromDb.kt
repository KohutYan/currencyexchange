package com.kohutyan.currency_exchange.domain.features.balance.use_cases

import com.kohutyan.currency_exchange.core.domain.use_cases.CoroutineUseCase
import com.kohutyan.currency_exchange.data.db.entities.toBalance
import com.kohutyan.currency_exchange.domain.features.balance.IBalanceRepository
import com.kohutyan.currency_exchange.domain.models.Balance
import javax.inject.Inject

class GetBalanceFromDb @Inject constructor(
    private val balanceRepository: IBalanceRepository
) : CoroutineUseCase<Balance, String>() {
    override suspend fun run(params: String): Balance {
        return balanceRepository.getBalanceFromDB(params).toBalance()
    }
}