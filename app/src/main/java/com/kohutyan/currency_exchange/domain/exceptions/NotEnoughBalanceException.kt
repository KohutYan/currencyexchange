package com.kohutyan.currency_exchange.domain.exceptions

import java.lang.Exception

class NotEnoughBalanceException(message: String?) : Exception(message)