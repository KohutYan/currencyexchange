package com.kohutyan.currency_exchange.domain.exceptions

import java.lang.Exception

class MissingBalanceException(message: String) : Exception(message)