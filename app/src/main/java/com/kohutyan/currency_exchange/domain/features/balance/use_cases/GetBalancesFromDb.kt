package com.kohutyan.currency_exchange.domain.features.balance.use_cases

import com.kohutyan.currency_exchange.core.domain.use_cases.CoroutineUseCase
import com.kohutyan.currency_exchange.data.db.entities.toBalance
import com.kohutyan.currency_exchange.domain.features.balance.IBalanceRepository
import com.kohutyan.currency_exchange.domain.models.Balance
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map
import javax.inject.Inject

class GetBalancesFromDb @Inject constructor(
    private val balanceRepository: IBalanceRepository
) : CoroutineUseCase<Flow<List<Balance>>, Unit>() {
    override suspend fun run(params: Unit): Flow<List<Balance>> {
        return balanceRepository.getBalancesFromDB().map { list -> list.map { it.toBalance() } }
    }
}