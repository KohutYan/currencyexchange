package com.kohutyan.currency_exchange.domain.features.currency.use_cases

import com.kohutyan.currency_exchange.core.domain.use_cases.CoroutineUseCase
import com.kohutyan.currency_exchange.domain.features.currency.ICurrencyRepository
import com.kohutyan.currency_exchange.domain.models.Currency
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import javax.inject.Inject

class GetCurrencyRate @Inject constructor(
    private val repository: ICurrencyRepository
) :
    CoroutineUseCase<Flow<List<Currency>>, Unit>() {
    override suspend fun run(params: Unit): Flow<List<Currency>> = flow {
        emit(repository.getCurrencyRate().rates.map { Currency(it.key, it.value) })
    }
}