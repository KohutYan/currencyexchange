package com.kohutyan.currency_exchange.domain.features.balance.use_cases

import com.kohutyan.currency_exchange.core.domain.use_cases.UseCase
import com.kohutyan.currency_exchange.domain.models.Currency
import javax.inject.Inject

class CountSellByReceive @Inject constructor() : UseCase<Double, SellByReceiveParams>() {
    override fun run(params: SellByReceiveParams): Double {
        with(params) {
            return (receive / receiveCurrency.value) * sellCurrency.value
        }
    }
}

data class SellByReceiveParams(
    val receive: Double,
    val sellCurrency: Currency,
    val receiveCurrency: Currency,
)
