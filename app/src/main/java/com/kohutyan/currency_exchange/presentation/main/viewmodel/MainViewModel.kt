package com.kohutyan.currency_exchange.presentation.main.viewmodel

import androidx.compose.runtime.MutableDoubleState
import androidx.compose.runtime.MutableState
import androidx.compose.runtime.mutableDoubleStateOf
import androidx.compose.runtime.mutableStateOf
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.kohutyan.currency_exchange.domain.features.balance.use_cases.AddBalanceToDb
import com.kohutyan.currency_exchange.domain.features.balance.use_cases.CountReceiveBySell
import com.kohutyan.currency_exchange.domain.features.balance.use_cases.CountSellByReceive
import com.kohutyan.currency_exchange.domain.features.balance.use_cases.ExchangeCurrency
import com.kohutyan.currency_exchange.domain.features.balance.use_cases.GetBalancesFromDb
import com.kohutyan.currency_exchange.domain.features.balance.use_cases.Params
import com.kohutyan.currency_exchange.domain.features.balance.use_cases.ReceiveBySellParams
import com.kohutyan.currency_exchange.domain.features.balance.use_cases.Result
import com.kohutyan.currency_exchange.domain.features.balance.use_cases.SellByReceiveParams
import com.kohutyan.currency_exchange.domain.features.currency.use_cases.GetCurrencyRate
import com.kohutyan.currency_exchange.domain.features.main.use_cases.IsFirstLaunch
import com.kohutyan.currency_exchange.domain.models.Balance
import com.kohutyan.currency_exchange.domain.models.Currency
import com.kohutyan.currency_exchange.utils.DEFAULT_BALANCE
import com.kohutyan.currency_exchange.utils.DEFAULT_CURRENCY
import com.kohutyan.currency_exchange.utils.network.DataState
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.onStart
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class MainViewModel @Inject constructor(
    private val getCurrencyRate: GetCurrencyRate,
    private val getBalancesFromDb: GetBalancesFromDb,
    private val addBalanceToDb: AddBalanceToDb,
    private val isFirstLaunch: IsFirstLaunch,
    private val countSellByReceive: CountSellByReceive,
    private val countReceiveBySell: CountReceiveBySell,
    private val exchangeCurrency: ExchangeCurrency,
) : ViewModel() {
    val balances: MutableState<DataState<List<Balance>>?> = mutableStateOf(null)

    val sellCurrencies: MutableState<DataState<List<Currency>>?> = mutableStateOf(null)
    val currencies: MutableState<DataState<List<Currency>>?> = mutableStateOf(null)

    val sellCurrency: MutableState<Currency?> = mutableStateOf(null)
    val receiveCurrency: MutableState<Currency?> = mutableStateOf(null)

    val exchangeResult: MutableState<Result?> = mutableStateOf(null)
    val error = MutableSharedFlow<String?>()

    val sellNumber: MutableDoubleState = mutableDoubleStateOf(0.0)
    val receiveNumber: MutableDoubleState = mutableDoubleStateOf(0.0)

    fun getCurrencyRate() {
        viewModelScope.launch {
            getCurrencyRate.run(Unit).onStart {
                currencies.value = DataState.Loading
            }.catch {
                currencies.value = DataState.Error(Exception(it))
                error.emit(it.message)
            }.collect {
                currencies.value = DataState.Success(it)

                if (balances.value is DataState.Success) {
                    sellCurrencies.value = DataState.Success(it.filter { currency ->
                        (balances.value as DataState.Success).data.map { balance -> balance.name }
                            .contains(currency.name)
                    })
                }
            }
        }
    }

    fun onResultDismiss() {
        exchangeResult.value = null
        sellNumber.doubleValue = 0.0
        receiveNumber.doubleValue = 0.0
    }

    fun watchBalances() {
        viewModelScope.launch {
            getBalancesFromDb.run(Unit).onStart {
                balances.value = DataState.Loading
            }.catch {
                balances.value = DataState.Error(Exception(it))
                error.emit(it.message)
            }.collect {
                balances.value =
                    DataState.Success(it.sortedByDescending { balance -> balance.value })

                if (currencies.value is DataState.Success) {
                    sellCurrencies.value =
                        DataState.Success((currencies.value as DataState.Success).data.filter { currency ->
                            it.map { balance -> balance.name }.contains(currency.name)
                        })
                }
            }
        }
    }

    fun setSellCurrency(currency: Currency) {
        sellCurrency.value = currency
        calculateReceiveBySell(sellNumber.doubleValue)
    }

    fun setReceiveCurrency(currency: Currency) {
        receiveCurrency.value = currency
        calculateReceiveBySell(sellNumber.doubleValue)
    }

    fun setSell(number: Double) {
        sellNumber.doubleValue = number
        calculateReceiveBySell(number)
    }

    fun setReceive(number: Double) {
        receiveNumber.doubleValue = number
        calculateSellByReceive(number)
    }

    fun initBalance() {
        viewModelScope.launch {
            if (isFirstLaunch.run(Unit)) {
                addBalanceToDb.run(Balance(DEFAULT_CURRENCY, DEFAULT_BALANCE))
            }
        }
    }

    fun exchange() {
        viewModelScope.launch {
            sellCurrency.value?.let { sellCurrency ->
                receiveCurrency.value?.let { receiveCurrency ->
                    exchangeCurrency.run(
                        Params(
                            sellNumber.doubleValue, sellCurrency, receiveCurrency
                        )
                    ).catch {
                        error.emit(it.message)
                    }.collect {
                        exchangeResult.value = it
                    }
                }
            }
        }
    }

    private fun calculateReceiveBySell(sell: Double) {
        sellCurrency.value?.let { sellCurrency ->
            receiveCurrency.value?.let { receiveCurrency ->
                val result = countReceiveBySell.run(
                    ReceiveBySellParams(
                        sell, sellCurrency, receiveCurrency
                    )
                )
                receiveNumber.doubleValue = result
            }
        }
    }

    private fun calculateSellByReceive(receive: Double) {
        sellCurrency.value?.let { sellCurrency ->
            receiveCurrency.value?.let { receiveCurrency ->
                val result = countSellByReceive.run(
                    SellByReceiveParams(
                        receive, sellCurrency, receiveCurrency
                    )
                )
                sellNumber.doubleValue = result
            }
        }
    }

}