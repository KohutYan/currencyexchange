package com.kohutyan.currency_exchange.presentation.main.ui

import android.widget.Toast
import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyRow
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Check
import androidx.compose.material3.*
import androidx.compose.runtime.*
import androidx.compose.ui.*
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.ColorFilter
import androidx.compose.ui.graphics.vector.ImageVector
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.*
import androidx.hilt.navigation.compose.hiltViewModel
import com.kohutyan.currency_exchange.R
import com.kohutyan.currency_exchange.core.ui.theme.*
import com.kohutyan.currency_exchange.domain.models.*
import com.kohutyan.currency_exchange.presentation.main.ui.components.*
import com.kohutyan.currency_exchange.presentation.main.viewmodel.MainViewModel
import com.kohutyan.currency_exchange.utils.EMPTY_STRING
import com.kohutyan.currency_exchange.utils.TWO_AFTER_COMMA
import com.kohutyan.currency_exchange.utils.network.DataState

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun MainScreen() {
    val viewModel = hiltViewModel<MainViewModel>()

    val context = LocalContext.current

    var balances by remember { mutableStateOf(listOf<Balance>()) }
    var currencies by remember { mutableStateOf(listOf<Currency>()) }
    var sellCurrencies by remember { mutableStateOf(listOf<Currency>()) }

    val sellCurrency = viewModel.sellCurrency.value
    val receiveCurrency = viewModel.receiveCurrency.value

    SideEffect {
        viewModel.apply {
            initBalance()
            watchBalances()
            getCurrencyRate()
        }
    }

    if (viewModel.balances.value is DataState.Success<List<Balance>>) {
        balances = (viewModel.balances.value as DataState.Success<List<Balance>>).data
    }

    if (viewModel.currencies.value is DataState.Success<List<Currency>>) {
        currencies = (viewModel.currencies.value as DataState.Success<List<Currency>>).data
    }

    if (viewModel.sellCurrencies.value is DataState.Success<List<Currency>>) {
        sellCurrencies = (viewModel.sellCurrencies.value as DataState.Success<List<Currency>>).data
    }

    LaunchedEffect(Unit) {
        viewModel.error.collect { message ->
            Toast.makeText(
                context,
                message,
                Toast.LENGTH_SHORT,
            ).show()
        }
    }

    viewModel.exchangeResult.value?.let { result ->
        ExchangeAlertDialog(
            onDismissRequest = { viewModel.onResultDismiss() },
            dialogTitle = stringResource(id = R.string.exchange),
            dialogText = stringResource(
                id = R.string.result_desc, result.sell, result.receive, result.fee
            ),
            onConfirmation = {
                viewModel.onResultDismiss()
            },
            icon = Icons.Default.Check
        )
    }

    Scaffold(
        floatingActionButtonPosition = FabPosition.Center,
        floatingActionButton = {
            Button(shape = FloatingActionButtonDefaults.largeShape,
                enabled = sellCurrency != null && receiveCurrency != null,
                modifier = Modifier.size(100.dp),
                colors = ButtonDefaults.buttonColors(
                    containerColor = PurpleGrey40,
                ),
                onClick = {
                    viewModel.exchange()
                }) {
                Image(
                    painterResource(id = R.drawable.round_autorenew_24),
                    modifier = Modifier
                        .size(50.dp)
                        .padding(4.dp),
                    contentDescription = EMPTY_STRING,
                    colorFilter = ColorFilter.tint(color = Color.White)
                )
            }
        },
        topBar = {
            CenterAlignedTopAppBar(colors = TopAppBarDefaults.mediumTopAppBarColors(
                containerColor = MaterialTheme.colorScheme.primaryContainer,
                titleContentColor = MaterialTheme.colorScheme.primary,
            ), modifier = Modifier.height(48.dp), title = {
                Box(Modifier.fillMaxHeight(), contentAlignment = Alignment.Center) {
                    Text(stringResource(id = R.string.app_name), fontSize = 16.sp)
                }
            })
        },
    ) { innerPadding ->
        Column(
            modifier = Modifier.padding(innerPadding),
        ) {
            Text(
                text = stringResource(id = R.string.my_balances).uppercase(),
                Modifier.padding(16.dp),
                color = MaterialTheme.colorScheme.secondary
            )
            LazyRow(
                contentPadding = PaddingValues(horizontal = 16.dp),
                horizontalArrangement = Arrangement.spacedBy(16.dp),
            ) {
                items(balances.size) { index ->
                    val balance = balances[index]
                    BalanceItem(balance.name, balance.value)
                }
            }
            Text(
                text = stringResource(id = R.string.currency_exchange).uppercase(),
                Modifier.padding(top = 32.dp, start = 16.dp, end = 16.dp),
                color = MaterialTheme.colorScheme.secondary
            )
            SellCurrency(value = viewModel.sellNumber.doubleValue,
                selected = sellCurrency,
                currencies = sellCurrencies,
                onValueChange = { viewModel.setSell(it) },
                onCurrencyChange = {
                    viewModel.setSellCurrency(it)
                })
            Divider(Modifier.padding(start = 72.dp, end = 16.dp))
            ReceiveCurrency(
                value = viewModel.receiveNumber.doubleValue,
                selected = receiveCurrency,
                currencies = currencies,
                onValueChange = { viewModel.setReceive(it) },
                onCurrencyChange = {
                    viewModel.setReceiveCurrency(it)
                },
            )
        }
    }
}

@Composable
fun BalanceItem(name: String, value: Double) {
    Text(
        text = "$name ${String.format(TWO_AFTER_COMMA, value)}"
    )
}

@Composable
fun ExchangeAlertDialog(
    onDismissRequest: () -> Unit,
    onConfirmation: () -> Unit,
    dialogTitle: String,
    dialogText: String,
    icon: ImageVector,
) {
    AlertDialog(icon = {
        Icon(icon, contentDescription = "Example Icon")
    }, title = {
        Text(text = dialogTitle)
    }, text = {
        Text(text = dialogText)
    }, onDismissRequest = {
        onDismissRequest()
    }, confirmButton = {
        TextButton(onClick = {
            onConfirmation()
        }) {
            Text("Confirm")
        }
    })
}
