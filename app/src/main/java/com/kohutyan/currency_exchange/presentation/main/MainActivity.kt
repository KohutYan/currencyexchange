package com.kohutyan.currency_exchange.presentation.main

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import com.kohutyan.currency_exchange.core.ui.theme.CurrencyExchangeTheme
import com.kohutyan.currency_exchange.presentation.main.ui.MainScreen
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            CurrencyExchangeTheme {
                MainScreen()
            }
        }
    }
}

