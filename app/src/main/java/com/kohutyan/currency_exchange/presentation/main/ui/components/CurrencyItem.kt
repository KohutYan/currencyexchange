package com.kohutyan.currency_exchange.presentation.main.ui.components

import androidx.annotation.DrawableRes
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material3.DropdownMenuItem
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.ExposedDropdownMenuBox
import androidx.compose.material3.Text
import androidx.compose.material3.TextField
import androidx.compose.material3.TextFieldDefaults
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.ColorFilter
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.unit.dp
import com.kohutyan.currency_exchange.R
import com.kohutyan.currency_exchange.core.ui.theme.Pink40
import com.kohutyan.currency_exchange.core.ui.theme.Purple40
import com.kohutyan.currency_exchange.domain.models.Currency
import com.kohutyan.currency_exchange.utils.EMPTY_STRING
import com.kohutyan.currency_exchange.utils.TWO_AFTER_COMMA

@Composable
fun SellCurrency(
    value: Double?,
    selected: Currency?,
    currencies: List<Currency>,
    onCurrencyChange: (Currency) -> Unit,
    onValueChange: (Double) -> Unit
) {
    CurrencyItem(
        value = value,
        painterResource = R.drawable.round_arrow_upward_24,
        iconBgColor = Pink40,
        onCurrencyChange = onCurrencyChange,
        onValueChange = onValueChange,
        currencies = currencies,
        selected = selected,
        name = stringResource(id = R.string.sell)
    )
}


@Composable
fun ReceiveCurrency(
    value: Double?,
    selected: Currency?,
    currencies: List<Currency>,
    onCurrencyChange: (Currency) -> Unit,
    onValueChange: (Double) -> Unit
) {
    CurrencyItem(
        value = value,
        painterResource = R.drawable.round_arrow_downward_24,
        iconBgColor = Purple40,
        onCurrencyChange = onCurrencyChange,
        onValueChange = onValueChange,
        currencies = currencies,
        selected = selected,
        name = stringResource(id = R.string.receive)
    )
}

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun CurrencyItem(
    @DrawableRes painterResource: Int,
    iconBgColor: Color,
    name: String,
    value: Double?,
    selected: Currency?,
    currencies: List<Currency>,
    onCurrencyChange: (Currency) -> Unit,
    onValueChange: (Double) -> Unit
) {
    var text by remember { (mutableStateOf(EMPTY_STRING)) }
    var isExpanded by remember { (mutableStateOf(false)) }

    if ((text.toDoubleOrNull() ?: 0.0) != value) {
        text = if (value != 0.0) String.format(TWO_AFTER_COMMA, value) else EMPTY_STRING
    }

    Row(
        verticalAlignment = Alignment.CenterVertically,
        modifier = Modifier.padding(vertical = 6.dp, horizontal = 16.dp)
    ) {
        Image(
            painterResource(id = painterResource),
            modifier = Modifier
                .size(38.dp)
                .background(color = iconBgColor, shape = CircleShape)
                .padding(4.dp),
            contentDescription = EMPTY_STRING,
            colorFilter = ColorFilter.tint(color = Color.White)
        )
        Box(modifier = Modifier.width(16.dp))
        Text(text = name)
        Spacer(Modifier.weight(1f))
        TextField(modifier = Modifier.width(100.dp),
            value = text,
            placeholder = { Text(stringResource(id = R.string.value_placeholder)) },
            singleLine = true,
            colors = TextFieldDefaults.colors(
                focusedContainerColor = Color.Transparent,
                unfocusedContainerColor = Color.Transparent,
                focusedIndicatorColor = Color.Transparent,
                unfocusedIndicatorColor = Color.Transparent
            ),
            keyboardOptions = KeyboardOptions(keyboardType = KeyboardType.Number),
            onValueChange = { value ->
                text = if (value.isEmpty()) {
                    value
                } else {
                    when (value.toDoubleOrNull()) {
                        null -> text.trim()
                        else -> value.trim()
                    }
                }

                onValueChange(text.toDoubleOrNull() ?: 0.0)
            })
        Box(modifier = Modifier.width(80.dp)) {
            ExposedDropdownMenuBox(expanded = isExpanded, onExpandedChange = { isExpanded = it }) {
                TextField(
                    value = selected?.name ?: EMPTY_STRING,
                    colors = TextFieldDefaults.colors(
                        unfocusedContainerColor = Color.Transparent,
                        focusedContainerColor = Color.Transparent,
                    ),
                    onValueChange = {},
                    readOnly = true,
                    modifier = Modifier.menuAnchor()
                )

                ExposedDropdownMenu(
                    expanded = isExpanded,
                    onDismissRequest = { isExpanded = false }) {
                    for (item in currencies) {
                        DropdownMenuItem(text = { Text(text = item.name) }, onClick = {
                            onCurrencyChange(item)
                            isExpanded = false
                        })
                    }
                }
            }
        }
    }
}