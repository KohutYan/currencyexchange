package com.kohutyan.currency_exchange

import android.app.Application
import android.util.Log
import androidx.lifecycle.LifecycleObserver
import com.orhanobut.logger.AndroidLogAdapter
import com.orhanobut.logger.LogStrategy
import com.orhanobut.logger.Logger
import com.orhanobut.logger.PrettyFormatStrategy
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class CurrencyExchangeApp : Application(), LifecycleObserver {

    override fun onCreate() {
        super.onCreate()
        initLogger()
        instance = this
    }

    private fun initLogger() {
        val prefix = Array(LOGGER_PREFIX_SIZE) { LOGGER_PREFIX }
        prefix[1] = LOGGER_PREFIX
        var index = 0
        val logStrategy = LogStrategy { priority, tag, message ->
            index = index xor 1
            Log.println(priority, prefix[index] + tag, message)
        }

        val formatStrategy = PrettyFormatStrategy.newBuilder()
            .showThreadInfo(true)  // (Optional) Whether to show thread info or not. Default true
            .methodCount(LOGGER_METHOD_COUNT)         // (Optional) How many method line to show. Default 2
            .methodOffset(LOGGER_METHOD_OFFSET)        // (Optional) Hides internal method calls up to offset. Default 5
            .logStrategy(logStrategy) // (Optional) Changes the log strategy to print out. Default LogCat
            .tag(LOGGER)   // (Optional) Global tag for every log. Default PRETTY_LOGGER
            .build()

        Logger.addLogAdapter(AndroidLogAdapter(formatStrategy))
    }

    companion object {
        lateinit var instance: CurrencyExchangeApp

        private const val LOGGER_PREFIX_SIZE = 2
        private const val LOGGER_PREFIX = ". "
        private const val LOGGER_METHOD_COUNT = 6
        private const val LOGGER_METHOD_OFFSET = 0
        private const val LOGGER = "PrettyLogger"

    }
}